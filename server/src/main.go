package main

import (
	"goref/src/modules/upload"
	"goref/src/modules/file"
	"github.com/labstack/echo/v4"
);

func main() {
	file.InitDataDirectory();

	// init Echo
	e := echo.New();

	// Middleware
	// e.Use(middleware.CORS());

	// Declare routes
	e.POST("/api/upload", upload.UploadFile);
	e.DELETE("/api/upload", upload.DeleteFile);
	e.GET("/api/upload", upload.GetFiles);

	// Serve static files
	e.Static("/", "../client/dist");
	e.Static("/data", file.DataDirectory);

	// Start server
	e.Logger.Fatal(e.Start(":8080"));
}
