import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Upload from "../views/Upload.vue";
import Home from "../views/Home.vue";

const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        name: "Home",
        component: Home
    },
  {
    path: "/upload",
    name: "Upload",
    component: Upload
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

export default router;
